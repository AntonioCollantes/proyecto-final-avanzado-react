import Link from "next/link";

const Navbar = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary" >
      <div className="container">

        <Link href="/">
          <a className="navbar-brand">
            <img src="https://marcas-logos.net/wp-content/uploads/2020/01/Batman-Logo.png" width="45" alt="" class="d-inline-block align-middle mr-2" />
            <span className="text-uppercase font-weight-bold">Portfolio</span>
          </a>
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <Link href="/blog">
                <a className="nav-link active" aria-current="page">
                <span className="text-uppercase font-weight-bold">Blog</span>
                </a>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
