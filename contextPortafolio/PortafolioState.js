import React, { useReducer } from 'react';
import MyPortafolioContext from './PortafolioContex';
import reducePortafolio from './PortafolioReduce';
import axios from 'axios';

const apiPortafolio = "https://61d7b0c135f71e0017c2ebd0.mockapi.io/api/v1/reactAvanzado";

const PortafolioState = ({ children }) => {

    const initialState = {
        listaExperiencias: [],
        listaHabilidades: [],
        listaProyectos: [],
        listaBlog: [],
        cabecera: {}
    };

    const obtenerExperiencias = async () => {
        const urlExperiencias = "/experiencias";
        try {
            let listExperiencias = [];
            await axios.get(`${apiPortafolio}${urlExperiencias}`).then(
                res => {
                    listExperiencias = res.data;
                });
            dispatch({
                type: 'LIST_EXPERIENCIAS',
                payload: listExperiencias,
            });
        } catch (error) {
            console.log(error);
        }
    };

    const obtenerHabilidades = async () => {
        const urlHabilidades = "/habilidades";
        try {
            let listHabilidades = [];
            await axios.get(`${apiPortafolio}${urlHabilidades}`).then(
                res => {
                    listHabilidades = res.data;
                });
            dispatch({
                type: 'LIST_HABILIDADES',
                payload: listHabilidades,
            });
        } catch (error) {
            console.log(error);
        }
    };

    const obtenerProyectos = async () => {
        const urlProyectos = "/proyectos";
        try {
            let listProyectos = [];
            await axios.get(`${apiPortafolio}${urlProyectos}`).then(
                res => {
                    listProyectos = res.data;
                });
            dispatch({
                type: 'LIST_PROYECTOS',
                payload: listProyectos,
            });
        } catch (error) {
            console.log(error);
        }
    };

    const obtenerBlogs = async () => {
        const urlBlog = "https://61a9072233e9df0017ea3c71.mockapi.io/universidad/publicaciones";
        try {
            let listBlog = [];
            await axios.get(`${urlBlog}`).then(
                res => {
                    listBlog = res.data;
                });
            dispatch({
                type: 'LIST_BLOG',
                payload: listBlog,
            });
        } catch (error) {
            console.log(error);
        }
    };

    const obtenerCabecera = async (id) => {
        const urlCabecera = "/cabecera";
        try {
            let datosPersonales;
            await axios.get(`${apiPortafolio}${urlCabecera}/${id}`).then(
                res => {
                    datosPersonales = res.data;
                });
            dispatch({
                type: 'GET_CABECERA',
                payload: datosPersonales,
            });
        } catch (error) {
            console.log(error);
        }
    };

    const [state, dispatch] = useReducer(reducePortafolio, initialState);

    return (
        <MyPortafolioContext.Provider
            value={
                {
                    listaExperiencias: state.listaExperiencias,
                    listaHabilidades: state.listaHabilidades,
                    listaProyectos: state.listaProyectos,
                    cabecera: state.cabecera,
                    listaBlog: state.listaBlog,
                    obtenerExperiencias,
                    obtenerHabilidades,
                    obtenerProyectos,
                    obtenerCabecera,
                    obtenerBlogs
                }
            }
        >
            {children}
        </MyPortafolioContext.Provider>
    );
};

export default PortafolioState;