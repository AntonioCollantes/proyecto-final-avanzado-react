const reducePortafolio = (state, option) => {
    switch (option.type) {
        case 'LIST_EXPERIENCIAS':
            return {
                ...state,
                listaExperiencias: option.payload,
            };
        case 'LIST_HABILIDADES':
            return {
                ...state,
                listaHabilidades: option.payload,
            };
        case 'LIST_PROYECTOS':
            return {
                ...state,
                listaProyectos: option.payload,
            };
        case 'GET_CABECERA':
            return {
                ...state,
                cabecera: option.payload,
            };
        case 'LIST_BLOG':
                return {
                    ...state,
                    listaBlog: option.payload,
                };
        default:
            return state;
    }
}

export default reducePortafolio;