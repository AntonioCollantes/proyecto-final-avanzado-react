export const myInfo = {
  nombre: "Juan Antonio Collantes Mejía",
  cargo: "Programador Java",
  descripcion: "Ingeniero de Computación y Sistemas, responsable, disciplinado con objetivos claros, inclinación por la investigación y el software libre y con gran disposición para trabajar en equipo para el logro de resultados."
}

export const habilidades = [
  {
    habilidad: "Java",
    porcentaje: 100,
  },
  {
    habilidad: "Spring",
    porcentaje: 80,
  },
  {
    habilidad: "Micro Servicios",
    porcentaje: 80,
  },
  {
    habilidad: "PL-SQL",
    porcentaje: 85,
  },
  {
    habilidad: "PHP",
    porcentaje: 30,
  },
  {
    habilidad: "SQL SERVER",
    porcentaje: 75,
  },
  {
    habilidad: "React.js",
    porcentaje: 60,
  },
];

export const experiencias = [
  {
    compania: "Software Enterprise Services",
    descripcion:
      "Desarrollo de Microservicios Para los Países: Colómbia, Chile, Costa Rica y Panamá",
    desde: 2017,
    hasta: 2022,
  },
  {
    compania: "Synopsis S.A.",
    descripcion:
      "Rediseño y migración del sistema Telebanking",
    desde: 2017,
    hasta: 2017,
  },
  {
    compania: "B-IT Solutions S.A.C",
    descripcion:
      "Desarrollo de los módulos de los distintos proyectos tales como ADMWEB, CONCON y RENIPRESS",
    desde: 2016,
    hasta: 2016
  },
  {
    compania: "Everis Perú S.AC",
    descripcion:
      "proyecto RNP V5.0 (Registro nacional de Proveedores)",
    desde: 2015,
    hasta: 2015
  },
];

export const proyectos = [
  {
    nombre: "Scotiabank",
    descripcion:
      "Desarrollo de Micro Servicios.",
    imagen: "portfolio1.jpeg",
  },
  {
    nombre: "ProEmpresa",
    descripcion:
      "Desarrollo de servicios Rest.",
      imagen: "portfolio2.jpg",
  },
  {
    nombre: "OSCE",
    descripcion:
      "Aplicativo Web regitro nacional de proveedores.",
    imagen: "portfolio3.png",
  },
  {
    nombre: "Claro",
    descripcion:
      "Desarrollo de archivos XSLT.",
      imagen: "portfolio4.png",
  },
  {
    nombre: "SUSALUD",
    descripcion:
      "Implementación de los módulos para los proyectos ADMWEB, CONCON y RENIPRESS",
    imagen: "portfolio5.jpeg",
  },
  {
    nombre: "REPSOL",
    descripcion:
      "Desarrollo de aplicativo par registro de clientes y proveedores",
    imagen: "portfolio6.jpeg",
  },
];

export const publicaciones = [
  {
    titulo: "React",
    contenido:
      "React te ayuda a crear interfaces de usuario interactivas de forma sencilla. Diseña vistas simples para cada estado en tu aplicación, y React se encargará de actualizar y renderizar de manera eficiente los componentes correctos cuando los datos cambien.",
    imagenURL:
      "https://sigdeletras.com/images/blog/202004_react_leaflet/react.png",
  },
  {
    titulo: "JAVA",
    contenido:
      "El lenguaje de programación Java fue desarrollado originalmente por James Gosling, de Sun Microsystems (constituida en 1983 y posteriormente adquirida el 27 de enero de 2010 por la compañía Oracle),4​ y publicado en 1995 como un componente fundamental de la plataforma Java de Sun Microsystems. Su sintaxis deriva en gran medida de C y C++, pero tiene menos utilidades de bajo nivel que cualquiera de ellos.",
    imagenURL:
      "https://habrastorage.org/r/w780/getpro/habr/upload_files/325/351/f3a/325351f3aafa92e2e2d755755bcb391d.png",
  },
  {
    titulo: "PLSQL",
    contenido:
      "PL/SQL hereda la robustez, la seguridad y la portabilidad de la base de datos PL/SQL es un lenguaje de procedimiento diseñado específicamente para abarcar sentencias SQL dentro de su sintaxis. El servidor de Oracle Database compila las unidades de programa PL/SQL y se almacenan dentro de la base de datos. Y en tiempo de ejecución, tanto PL/SQL como SQL se ejecutan dentro del mismo proceso de servidor, brindando una eficiencia óptima.",
    imagenURL:
      "https://media.istockphoto.com/photos/inscription-against-laptop-and-code-background-learn-plsql-language-picture-id1299051269?k=20&m=1299051269&s=612x612&w=0&h=PjHFlRBOYcvnT9xg6Q0VLfh-EbcQG7ts2zd27I73Q-E=",
  },
];
