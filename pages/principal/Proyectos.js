import React from 'react';
import Link from "next/link";

const Proyectos = ({proyectos}) => {
    return (
        <div className="row">
            <div className="col-md-12">
                <div className="card card-body bg-primary">
                    <div className="row">
                        <div className="col-md-12 my-2">
                            <h1 className="text-center text-light">Portfolio</h1>
                        </div>
                        {proyectos.map(({ nombre, descripcion, imagen }, index) => (
                            <div className="col-md-4 p-2" key={index}>
                                <div className="card h-100">
                                    <div className="overflow">
                                        <img
                                            src={`/${imagen}`}
                                            alt=""
                                            className="card-img-top"
                                        />
                                    </div>
                                    <div className="card-body">
                                        <h3>{nombre}</h3>
                                        <p>{descripcion}</p>
                                    </div>
                                </div>
                            </div>
                        ))}

                        <div className="col-md-12 mt-4">
                            <div className="text-center">
                                <Link href="/portfolio">
                                    <a className="btn btn-outline-light">Más Proyectos</a>
                                </Link>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
};

export default Proyectos;