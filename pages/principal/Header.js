const Cabecera = ({ info }) => {
    return (
        <div className="col-md-12">
            <div className="card card-body bg-primary text-light animate__animated animate__fadeIn">
                <div className="row">
                    <div className="col-md-4">
                        <img src={info.foto} alt="" className="img-fluid" />
                    </div>
                    <div className="col-md-8">
                        <h1>{info.nombre}</h1>
                        <h3>{info.cargo}</h3>
                        <p>{info.descripcion}</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Cabecera;