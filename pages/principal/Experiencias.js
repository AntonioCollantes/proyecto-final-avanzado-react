import Link from "next/link";

const Experiencias = ({experiencias}) => {
    return (
        <div className="col-md-8 py-2">
            <div className="card bg-light animate__animated animate__fadeInRight">
                <div className="card-body" >
                    <h1>Experiencias</h1>
                    <ul>
                        {experiencias.map(({ compania, desde, hasta, descripcion }, index) => (
                            <li key={index}>
                                <h3>{compania}</h3>
                                <h5>
                                    {desde} {desde ? `- ${hasta}` : "- current"}
                                </h5>
                                <p>
                                    {descripcion}
                                </p>
                            </li>
                        ))}
                    </ul>
                    <Link href="/Experiencias">
                        <a>Ver Más</a>
                    </Link>
                </div>
            </div>
        </div>
    );
};

export default Experiencias;