const Habilidades = ({habilidades}) => {
    return (
        <div className="col-md-4 py-2">
            <div className="card bg-light animate__animated animate__fadeInLeft">
                <div className="card-body">
                    <h1>Habilidades</h1>

                    {habilidades.map(({ habilidad, porcentaje }, i) => (
                        <div className="py-3" key={i}>
                            <h5>{habilidad}</h5>
                            <div className="progress ">
                                <div
                                    className="progress-bar"
                                    role="progressbar"
                                    style={{ width: `${porcentaje}%` }}
                                    aria-valuenow="50"
                                    aria-valuemin="0"
                                    aria-valuemax="100"
                                ></div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>

    );
};

export default Habilidades;