import Layout from "../components/Layout";
import Cabecera from "./principal/Header";
import Experiencias from "./principal/Experiencias";
import Habilidades from "./principal/Habilidades";
import Proyectos from "./principal/Proyectos";
import MyPortafolioContext from "../contextPortafolio/PortafolioContex";
import React, { useContext, useEffect } from 'react';

const Index = () => {

  const { 
    obtenerCabecera, cabecera, 
    obtenerHabilidades, listaHabilidades, 
    obtenerExperiencias, listaExperiencias ,
    obtenerProyectos, listaProyectos
  } = useContext(MyPortafolioContext);

  useEffect(() => {
    obtenerCabecera(1);
    obtenerHabilidades();
    obtenerExperiencias();
    obtenerProyectos();
  }, []);

  return (
    <Layout>
      <header className="row">
        <Cabecera info={cabecera} />
      </header>
      <section className="row">
        <Experiencias experiencias = {listaExperiencias}/>
        <Habilidades habilidades = {listaHabilidades}/>
      </section>
      <section>
        <Proyectos proyectos = {listaProyectos}/>
      </section>
    </Layout>
  );
}

export default Index;