import Layout from "../../components/Layout";
import PostCard from "./PostCard";
import MyPortafolioContext from "../../contextPortafolio/PortafolioContex";
import React, { useContext, useEffect } from 'react';

const blog = () => {

  const {obtenerBlogs, listaBlog} = useContext(MyPortafolioContext);

  useEffect(() => {
    obtenerBlogs();
  }, []);

  return (
    <Layout title="Mi Blog" footer={false} dark>
      <div className="row">
        {listaBlog.map((publicaciones, i) => (
          <PostCard publicacion={publicaciones} key={i} />
        ))}
      </div>
    </Layout>
  );
};

export default blog;
