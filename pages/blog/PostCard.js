import Link from "next/link";

const PostCard = ({ publicacion }) => (
    <div className="col-md-4">
      <div className="card">
        <div className="overflow">
          <img src={publicacion.imagenURL} alt="" className="card-img-top" />
        </div>
        <div className="card-body">
          <h1>{publicacion.titulo}</h1>
          <p>{publicacion.contenido}</p>
          <Link href={`/blog/post?title=${publicacion.titulo}`}>
            <button className="btn btn-light">Leer</button>
          </Link>
        </div>
      </div>
    </div>
  );

  export default PostCard;