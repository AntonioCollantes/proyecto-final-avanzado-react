import Layout from "../../components/Layout";
import { useRouter } from "next/router";
import { publicaciones } from "../../datos/profile";

const Post = () => {
  const router = useRouter();

  const publicacion = publicaciones.filter(
    (publicaciones) => publicaciones.titulo === router.query.title
  )[0];

  return (
    <Layout title={router.query.title} footer={false}>
      <div className="text-center">
        <img
          src={publicacion.imagenURL}
          alt=""
          style={{ width: "50%" }}
          className="img-fluid"
        />
        <p className="p-4">{publicacion.contenido}</p>
      </div>
    </Layout>
  );
};

export default Post;
