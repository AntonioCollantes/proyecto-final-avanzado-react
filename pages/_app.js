import 'bootswatch/dist/cosmo/bootstrap.min.css'
import 'animate.css/animate.min.css'
import '../global.css'
import PortafolioState from '../contextPortafolio/PortafolioState';

function MyApp({ Component, pageProps }) {
  return (
    <PortafolioState>
      <Component {...pageProps} />
    </PortafolioState>
  );
}

export default MyApp;