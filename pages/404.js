import Link from "next/link";
import Layout from "../components/Layout";

const error404 = () => (
  <Layout title="Página no existe">
    <div className="text-center">
      <h1 className="display-1">404</h1>
      <p>
        La Página no existe. retorne a {" "}
        <Link href="/">
          <a>Portafólio</a>
        </Link>
      </p>
    </div>
  </Layout>
);

export default error404;
